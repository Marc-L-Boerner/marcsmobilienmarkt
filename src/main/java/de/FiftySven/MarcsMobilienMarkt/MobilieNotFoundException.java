package de.FiftySven.MarcsMobilienMarkt;

class MobilieNotFoundException extends RuntimeException {
    MobilieNotFoundException(Long id) {
        super("Could not find mobilie " + id);
    }
}

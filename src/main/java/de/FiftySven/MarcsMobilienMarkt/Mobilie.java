package de.FiftySven.MarcsMobilienMarkt;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Mobilie {
    private @Id @GeneratedValue Long id;
    private String Typbezeichnung;
    private int Preis;
    private int Leistung;
    private int kmStand;
    Mobilie() {}
    Mobilie(String Typbezeichnung, int Preis, int Leistung, int kmStand) {
        this.Typbezeichnung = Typbezeichnung;
        this.Preis = Preis;
        this.Leistung = Leistung;
        this.kmStand = kmStand;
    }

    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }

}

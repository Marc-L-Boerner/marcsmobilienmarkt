package de.FiftySven.MarcsMobilienMarkt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarcsMobilienMarktApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarcsMobilienMarktApplication.class, args);
	}

}

package de.FiftySven.MarcsMobilienMarkt;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
class MobilieController {
    private final MobilienRepository repository;
    MobilieController(MobilienRepository repository) {
        this.repository = repository;
    }
    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/mobilie")
    CollectionModel<EntityModel<Mobilie>> all() {
        List mobilien = repository.findAll().stream()
                .map(mobilie -> EntityModel.of(mobilie,
                        linkTo(methodOn(MobilieController.class).one(mobilie.getId())).withSelfRel(),
                        linkTo(methodOn(MobilieController.class).all()).withRel("mobilie")))
                .collect(Collectors.toList());
        return CollectionModel.of(mobilien, linkTo(methodOn(MobilieController.class).all()).withSelfRel());
    }

    @GetMapping("/mobilie/{id}")
    EntityModel<Mobilie> one(@PathVariable Long id) {
        Mobilie m = repository.findById(id)
                .orElseThrow(() -> new MobilieNotFoundException(id));
        return EntityModel.of(m,
                linkTo(methodOn(MobilieController.class).one(id)).withSelfRel(),
                linkTo(methodOn(MobilieController.class).all()).withRel("mobilie"));
    }
}

package de.FiftySven.MarcsMobilienMarkt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);
    @Bean
    CommandLineRunner initDatabase(MobilienRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new Mobilie("Audi A4 B6", 3000, 131, 180000)));
	        log.info("Preloading " + repository.save(new Mobilie("Audi A4 B7", 5000, 140, 130000)));
        };
    }
}

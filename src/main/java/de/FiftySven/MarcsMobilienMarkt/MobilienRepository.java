package de.FiftySven.MarcsMobilienMarkt;

import org.springframework.data.jpa.repository.JpaRepository;

interface MobilienRepository extends JpaRepository<Mobilie, Long> {}
